const express = require("express");
// the taskController allows us to use the function defined inside it.

const taskController = require("../controllers/taskController"); //"taskController" can be changed

// allows access to http method middlewares that makes it easier to create routes for our application
const router = express.Router();

// Route to get all tasks
router.get("/", (req, res)=>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));	// "resultFromController" can be changed
})

// Route to create a Task
// localhost:3001/task it is the same with localhost:3001/task/
router.post("/", (req, res) =>{
	// the createTask function needs data from the request body, so we need it supply in the taskControlle.createTask(argument)
	// .createTask is being made in taskController
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to Delete a task
// `:` is an identifier that helps create a dynamic route
// ":id" is a wildcard where you can put the objectID as the value
//  ex: localhost:3000/task/:id or localhostL3000/tasks/12345

router.delete("/:id", (req, res)=>{
	//If information will be coming from the URl, the data can be accessed from the request "params" property.
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Route to update a task
router.patch("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/:id", (req, res) =>{
	taskController.getOneTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) =>{
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// use "module.exports" to export the router object to be use in the server
module.exports = router;