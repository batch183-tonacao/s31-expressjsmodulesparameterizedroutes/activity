// npm init -y
// npm install mongoose

// set-up dependencies
const express = require("express");
const mongoose = require("mongoose");

// this allows us to use all the routes defined in the "taskRoute.js"
const taskRoute = require("./routes/taskRoute")


//Server set-up
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@coursebooking.n7rpo.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

let db = mongoose.connection;
// Notify on error
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", ()=> console.log("We're connected to the cloud database."));

// Add the task route
// allows all the task routes created in the "taskRoute.js" file to use "/tasks" route

// localhost:3001/task/
app.use("/tasks", taskRoute);

// Server listening
app.listen(port, ()=> console.log(`Now listening to port: ${port}`));

/*
Separation of Concerns

Model folder
	- contains the object schemas and defines the object structure and content

Controllers folder
	- contains the function and business logic of our JS application 
	- all the operations it can do will be placed here

Routes folder
	- contains all the endpoints and assign the http methods for our application
	app.get("/",)
	- we separate the routes such that the server/"index.js" only contains information on the server

JS modules
	require => to include a specific module/package

	export.module => to treat a value as a "package" that can be used by other files.

*/